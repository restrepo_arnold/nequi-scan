import React, { useRef, useEffect, useState } from 'react';
import Quagga from 'quagga';
import axios from 'axios';
import Loading from './Loading';
import FakeData from '../api.json';

const Scann = () => {
  const barCodeRef = useRef(null);
  const [resultCode, setResultCode] = useState('');
  const [dataRouter, setDataRouter] = useState(undefined);
  const [activeRoute, setActiveRoute] = useState(false);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    Quagga.init(
      {
        inputStream: {
          constraints: {
            width: 720,
            height: 480,
          },
          name: 'Live',
          type: 'LiveStream',
          target: barCodeRef.current,
        },
        decoder: {
          readers: ['ean_reader'],
        },
      },
      function (error) {
        if (error) {
          console.log(error);
          return;
        }
        console.log('Iniciado correctamente');
        Quagga.start();
      }
    );

    Quagga.onDetected((data) => {
      setResultCode(data.codeResult.code);
      // Imprimimos todo el data para que puedas depurar
      console.log(data);
    });

    Quagga.onProcessed(function (result) {
      var drawingCtx = Quagga.canvas.ctx.overlay,
        drawingCanvas = Quagga.canvas.dom.overlay;

      if (result) {
        if (result.boxes) {
          drawingCtx.clearRect(
            0,
            0,
            parseInt(drawingCanvas.getAttribute('width')),
            parseInt(drawingCanvas.getAttribute('height'))
          );
          result.boxes
            .filter(function (box) {
              return box !== result.box;
            })
            .forEach(function (box) {
              Quagga.ImageDebug.drawPath(box, { x: 0, y: 1 }, drawingCtx, {
                color: 'green',
                lineWidth: 2,
              });
            });
        }

        if (result.box) {
          Quagga.ImageDebug.drawPath(result.box, { x: 0, y: 1 }, drawingCtx, {
            color: '#00F',
            lineWidth: 2,
          });
        }

        if (result.codeResult && result.codeResult.code) {
          Quagga.ImageDebug.drawPath(
            result.line,
            { x: 'x', y: 'y' },
            drawingCtx,
            { color: 'red', lineWidth: 3 }
          );
        }
      }
    });
  }, []);

  useEffect(() => {
    const getRouteData = async (id) => {
      setLoading(true);
      const URL = `https://yrlnol87me.execute-api.us-east-2.amazonaws.com/dev/?barcode=${id}`;
      const response = await axios.get(URL);
      return response;
    };

    getRouteData(resultCode)
      .then((response) => {
        setDataRouter(response.data.body);
      })
      .catch((error) => {
        console.log('Error', error);
      });

    setLoading(false);
    setActiveRoute(false);
  }, [activeRoute]);

  const searchRoute = () => setActiveRoute(true);

  return (
    <>
      <section className='BannerScann'>
        <h2>Bienvenido al Scanner de Tarjetas Nequi</h2>
        <p>
          Acerca tu código a la cámara <i className='fa fa-camera' />
        </p>
      </section>
      <section className='Code'>
        <h2 className='Code__title'>El Código de la Tarjeta es:</h2>
        <p className='Code__result'>{resultCode}</p>
        <button
          aria-label='Buscar Ruta de la Tarjeta'
          onClick={searchRoute}
          className='btn'
          disabled={barCodeRef ? '' : 'disabled'}
        >
          {loading ? 'Cargando...' : 'Buscar Ruta'}
        </button>
      </section>
      {}
      {dataRouter ? (
        <section className='Result'>
          <h3 className='Result__title'>Resultado:</h3>
          <ul className='Result__list'>
            <li>
              <b>Nombre: </b>
              {dataRouter.name}
            </li>
            <li>
              <b>Ciudad: </b>
              {dataRouter.city}
            </li>
            <li>
              <b>Dirección: </b>
              {dataRouter.address}
            </li>
            <li>
              <b>Teléfono: </b>
              {dataRouter.telephone}
            </li>
            <li>
              <b>Número de Ruta: </b>
              {dataRouter.num_route}
            </li>
          </ul>
        </section>
      ) : null}
      <section className='Scann' ref={barCodeRef} />
    </>
  );
};

export default Scann;
