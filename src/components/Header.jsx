import React from 'react';

const Header = () => (
  <header className='Header'>
    <img
      src='https://assets.quick.com.co/images/logos/quick-app.svg'
      alt='Logo Quick'
      className='Logo'
    />
    <h1 className='Title'>Quick Scann.</h1>
  </header>
);

export default Header;
