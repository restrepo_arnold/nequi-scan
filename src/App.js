import React from 'react';
import Header from './components/Header';
import Scann from './components/Scann';
import './App.css';

function App() {
  return (
    <div className='Wrapper'>
      <Header />
      <Scann />
    </div>
  );
}

export default App;
